/**
 * Author: Håvar Nøvik
 */

// only make the findtoolbarAutohide object if it's "undefined"
if (typeof findtoolbarAutohide == "undefined") {
    var findtoolbarAutohide = function() {

        var _container = gBrowser.tabContainer;
        var _findbar = null;

        /**
        * The function that will be called every time the "TabSelect" event is fired.
        * First the findbar of the previous tab is closed, and then the findbar of the
        * new tab is stored in "_findbar". Ready to be closed on the next tab change.
        */
        var _closeFindbar = function(event) {
            _findbar.close();
            _findbar = gBrowser.getFindBar();
        }

        return {
            /**
            * The constructor for the findtoolbarAutohide object.
            * It sets an event listner to the _container variable (this windows tab container).
            */
            init : function() {
                _findbar = gBrowser.getFindBar();
                _container.addEventListener("TabSelect", _closeFindbar, false);
            }
        }

    }();
    
    // call the findtoolbars init function on window load.
    window.addEventListener("load", findtoolbarAutohide.init, false);
};
